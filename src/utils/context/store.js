import { createContext, useEffect, useState } from "react";
import { subscriptionServer, userServer } from "../../server";

export const Store = createContext();

const GlobalStore = ({ children }) => {
  const [subscriptions, setSubscriptions] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function getSubscribe() {
      const result = await subscriptionServer();
      setSubscriptions(result);
    }

    async function getUsers() {
      const result = await userServer();
      setUsers(result);
    }
    getSubscribe();
    getUsers();
  }, []);

  return (
    <Store.Provider value={{ subscriptions, users }}>{children}</Store.Provider>
  );
};

export default GlobalStore;
