import logo from "./logo.svg";
import "./App.css";
import { useContext, useState } from "react";
import { Store } from "./utils/context/store";
import Subscribers from "./components/subscribers";

function App() {
  const { subscriptions, users } = useContext(Store);

  const [allUsersData, setAllUsersData] = useState(
    users.length > 0 ? users : []
  );
  // console.log(allUsersData);
  const [filterData, setFilterData] = useState("");
  const [plans, setPlan] = useState("");
  const [isActive, setIsActive] = useState("");
  const [sorting, setSorting] = useState("");

  let sort = "hello world";

  let data = 20;

  const packageList = [
    ...new Map(subscriptions.map((item) => [item["package"], item])).values(),
  ];

  // const sortingByName = function sortByName() {
  //   users.sort((a, b) => a.first_name.localeCompare(b.first_name));
  // };

  // console.log(sortingByName);

  // console.log(packageList);

  // console.log(users);

  const tableData = () => {
    return (
      <>
        <></>
      </>
    );
  };

  return (
    <div className="App">
      <Subscribers />
    </div>
  );
}

export default App;
